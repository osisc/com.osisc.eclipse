/*******************************************************************************
 * Copyright (c) 2016 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/

package com.osisc.swt.test;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.After;
import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.*;

/**
 * @author mattias
 */
public class AsyncExecTest {
    Display display;
    Shell shell;
    Throwable exception;

    @Before
    public void setup() {
        display = Display.getDefault();
        shell = new Shell(display);
        shell.open();
    }
    @After
    public void tearDown() {
        display.dispose();
    }

    // Problems:
    // Accessing disposed widget on UI thread.
    // Accessing disposed display after getDefault()
    // Creating a new display and setting worker to UI thread.
    void asyncExec0() {
        Display.getDefault().asyncExec(new Runnable() {
            public void run() {
                shell.getStyle();
            }
        });
    }

    // Problems:
    // Accessing disposed display after getDefault()
    // Creating a new display and setting worker to UI thread.
    void asyncExec1() {
        Display.getDefault().asyncExec(new Runnable() {
            public void run() {
                if (!shell.isDisposed()) {
                    shell.getStyle();
                }
            }
        });
    }

    // Problems:
    // Creating a new display and setting worker to UI thread.
    void asyncExec2() {
        try {
            Display.getDefault().asyncExec(new Runnable() {
                public void run() {
                    if (!shell.isDisposed()) {
                        shell.getStyle();
                    }
                }
            });
        } catch (SWTException e) {
            if (e.code != SWT.ERROR_DEVICE_DISPOSED) {
                throw e;
            }
        }
    }

    // Problems:
    // Accessing non-synchronized getDisplay() on worker thread.
    void asyncExec3() {
        try {
            shell.getDisplay().asyncExec(new Runnable() {
                public void run() {
                    if (!shell.isDisposed()) {
                        shell.getStyle();
                    }
                }
            });
        } catch (SWTException e) {
            if (e.code != SWT.ERROR_WIDGET_DISPOSED
                && e.code != SWT.ERROR_DEVICE_DISPOSED) {
                throw e;
            }
        }
    }

    // Correct?
    void asyncExec4() {
        try {
            display.asyncExec(new Runnable() {
                public void run() {
                    if (!shell.isDisposed()) {
                        shell.getStyle();
                    }
                }
            });
        } catch (SWTException e) {
            if (e.code != SWT.ERROR_DEVICE_DISPOSED) {
                throw e;
            }
        }
    }

    class Async extends Thread {
        public void run() {
            try {
                asyncExec4();
            } catch (Throwable e) {
                exception = e;
            }
        }
    }

    @Test
    public void widgetDisposedBeforeWorker() throws Throwable {
        Async async = new Async();

        shell.dispose();
        async.start();
        async.join();
        if (exception != null) {
            throw exception;
        }
        while(display.readAndDispatch()) {}
    }

    @Test
    public void widgetDisposedBeforeUI() throws Throwable {
        Async async = new Async();

        display.asyncExec(new Runnable() {
            public void run() {
                shell.dispose();
            }
        });
        async.start();
        async.join();
        if (exception != null) {
            throw exception;
        }
        while(display.readAndDispatch()) {}
    }

    @Test
    public void displayDisposedBeforeWorker() throws Throwable {
        Async async = new Async();

        display.dispose();
        async.start();
        async.join();
        if (exception != null) {
            throw exception;
        }
        assertSame(Thread.currentThread(), Display.getDefault().getThread());
    }
}
