/*******************************************************************************
 * Copyright (c) 2016-2020 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/

package com.osisc.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Widget;

/**
 * @author mattias
 */
public class SWTUtils {

    private static Runnable safeRunnable(final Widget widget, final Runnable runnable) {
        return new Runnable() {
            public void run() {
                if (!widget.isDisposed()) {
                    runnable.run();
                }
            }
        };
    }

    private static void filterException(SWTException e) {
        if (e.code != SWT.ERROR_DEVICE_DISPOSED) {
            throw e;
        }
    }

    public static void asyncExec(Display display, Widget widget, Runnable runnable) {
        try {
            display.asyncExec(safeRunnable(widget, runnable));
        } catch (SWTException e) {
            filterException(e);
        }
    }
    public static void syncExec(Display display, Widget widget, Runnable runnable) {
        try {
            display.syncExec(safeRunnable(widget, runnable));
        } catch (SWTException e) {
            filterException(e);
        }
    }
}
