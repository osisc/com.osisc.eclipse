/*******************************************************************************
 * Copyright (c) 2013-15 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/

package com.osisc.eclipse;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;

import com.osisc.eclipse.internal.PluginLog;

/**
 * A common base class for Eclipse plugins minimizing code duplication.
 * @author mattias
 */
public abstract class AbstractPlugin extends Plugin {
    private int logLevel = IStatus.WARNING;

    public abstract String id();

    public int logLevel() {
        return logLevel;
    }

    public void log(IStatus status) {
        PluginLog.log(logLevel(), id(), status);
    }
    public void log(int severity, String fmt, Object... args) {
        PluginLog.log(logLevel(), id(), severity, null, fmt, args);
    }
    public void log(int severity, Throwable e, String fmt, Object... args) {
        PluginLog.log(logLevel(), id(), severity, e, fmt, args);
    }

    public IStatus status(int severity, String msg) {
        return new Status(severity, id(), msg, null);
    }
    public IStatus status(int severity, Throwable e, String msg) {
        return new Status(severity, id(), msg, e);
    }
    public IStatus status(int severity, Enum code, String msg) {
        return new Status(severity, id(), code.ordinal(), msg, null);
    }
    public IStatus status(int severity, Enum code, Throwable e, String msg) {
        return new Status(severity, id(), code.ordinal(), msg, e);
    }
}
