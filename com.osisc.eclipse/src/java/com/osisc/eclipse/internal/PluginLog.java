/*******************************************************************************
 * Copyright (c) 2015 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/

package com.osisc.eclipse.internal;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.Bundle;

/**
 * Plugin log wrapper for use from AbstractPlugin and AbstractUIPlugin.
 * @author mattias
 */
public class PluginLog {
    private static ILog getLog(String id) {
        Bundle bundle = Platform.getBundle(id);
        return (bundle != null) ? Platform.getLog(bundle) : null;
    }

    public static void log(int level, String pluginId, int severity,
                           Throwable e, String fmt, Object... args) {
        if (severity >= level) {
            ILog log = getLog(pluginId);

            if (log != null) {
                log.log(new Status(severity, pluginId, String.format(fmt, args), e));
            }
        }
    }

    public static void log(int level, String pluginId, IStatus status) {
        if (status.getSeverity() >= level) {
            ILog log = getLog(pluginId);

            if (log != null) {
                log.log(status);
            }
        }
    }
}
