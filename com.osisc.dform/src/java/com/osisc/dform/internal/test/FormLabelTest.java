/*******************************************************************************
 * Copyright (c) 2017 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/

package com.osisc.dform.internal.test;

import static org.junit.Assert.assertEquals;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.osisc.dform.FormLabel;

/**
 * @author mattias
 */
public class FormLabelTest {
    Composite area;
    TestDialog dialog;
    private static Display display;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        display = new Display();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        display.dispose();
    }

    class TestDialog extends Dialog {
        public TestDialog() {
            super((Shell)null);
            setBlockOnOpen(false);
        }

        protected boolean isResizable() { return true; }

        protected Control createDialogArea(Composite parent) {
            area = (Composite)super.createDialogArea(parent);
            return area;
        }

        public int getReturnCode() {
            Shell shell = getShell();

            if (Boolean.getBoolean("com.osisc.junit.interactive")) {
                while (!shell.isDisposed()) {
                    if (!shell.getDisplay().readAndDispatch()) {
                        shell.getDisplay().sleep();
                    }
                }
            } else {
                while (shell.getDisplay().readAndDispatch());
            }
            return super.getReturnCode();
        }
    }

    @Before
    public void setUp() throws Exception {
        dialog = new TestDialog();
    }

    @After
    public void tearDown() throws Exception {
        if (dialog.getShell() != null) {
            dialog.getShell().dispose();
        }
    }

    @Test
    public void testSeparator() {
        dialog.open();
        dialog.getShell().setText("testSeparator");

        int separator = SWT.SEPARATOR;

        Label label00 = new Label(area, separator);
        Label label01 = new Label(area, separator | SWT.VERTICAL);
        Label label02 = new Label(area, separator | SWT.HORIZONTAL);

        FormLabel label10 = new FormLabel(area, separator);
        FormLabel label11 = new FormLabel(area, separator | SWT.VERTICAL);
        FormLabel label12 = new FormLabel(area, separator | SWT.HORIZONTAL);

        dialog.getShell().pack();

        assertEquals(Dialog.OK, dialog.getReturnCode());
    }

    @Test
    public void testSeparatorStyle() {
        dialog.open();
        dialog.getShell().setText("testSeparatorStyle");

        int separator = SWT.SEPARATOR | SWT.HORIZONTAL;

        Label label00 = new Label(area, separator);
        Label label01 = new Label(area, separator | SWT.SHADOW_IN);
        Label label02 = new Label(area, separator | SWT.SHADOW_OUT);

        FormLabel label10 = new FormLabel(area, separator);
        FormLabel label11 = new FormLabel(area, separator | SWT.SHADOW_IN);
        FormLabel label12 = new FormLabel(area, separator | SWT.SHADOW_OUT);

        dialog.getShell().pack();

        assertEquals(Dialog.OK, dialog.getReturnCode());
    }

    @Test
    public void testText() {
        dialog.open();
        dialog.getShell().setText("testText");

        Label label0 = new Label(area, 0);
        label0.setText("Label");

        FormLabel label1 = new FormLabel(area, 0);
        label1.setText("Label");

        dialog.getShell().pack();

        assertEquals(Dialog.OK, dialog.getReturnCode());
    }

    @Test
    public void testImage() {
        dialog.open();
        dialog.getShell().setText("testImage");
        assertEquals(Dialog.OK, dialog.getReturnCode());
    }

    @Test
    public void testTextAndImage() {
        dialog.open();
        dialog.getShell().setText("testTextAndImage");
        assertEquals(Dialog.OK, dialog.getReturnCode());
    }

    @Test
    public void testBackground() {
        dialog.open();
        dialog.getShell().setText("testBackground");
        assertEquals(Dialog.OK, dialog.getReturnCode());
    }

    @Test
    public void testBackgroundImage() {
        dialog.open();
        dialog.getShell().setText("testBackgroundImage");
        assertEquals(Dialog.OK, dialog.getReturnCode());
    }

    @Test
    public void testAlignment() {
        dialog.open();
        dialog.getShell().setText("testAlignment");
        assertEquals(Dialog.OK, dialog.getReturnCode());
    }
}
