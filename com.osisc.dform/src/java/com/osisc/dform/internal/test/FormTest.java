/*******************************************************************************
 * Copyright (c) 2016 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/

package com.osisc.dform.internal.test;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.osisc.dform.Form;
import com.osisc.dform.FormEditor;
import com.osisc.dform.FormItem;

import static org.junit.Assert.*;

/**
 * @author mattias
 */
public class FormTest {
    private static Display display;
    TestDialog dialog;
    Form form;
    FormItem docType, html, head, title, body, h1;
    FormEditor editor;

    static final String[] HTML_TAGS = {
        "DOCTYPE:", "html:", "head:", "title:", "body:", "h1:"
    };

    static final int[] HTML_INDENT = {
        0, 0, 1, 2, 1, 2
    };

    @BeforeClass
    public static void setupBeforeClass() {
        display = new Display();
    }

    @AfterClass
    public static void tearDownAfterClass() {
        display.dispose();
    }

    class TestDialog extends Dialog {
        public TestDialog() {
            super((Shell)null);
            setBlockOnOpen(false);
        }

        protected boolean isResizable() { return true; }

        protected Control createDialogArea(Composite parent) {
            form = new Form(parent, SWT.NONE);
            form.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

            docType = new FormItem(form, SWT.NONE);
            docType.setText("DOCTYPE:");
            Text docTypeText = new Text(docType.getContent(), SWT.BORDER);
            docTypeText.setText("html");
            docType.setEditor(docTypeText);

            html = new FormItem(form, SWT.NONE);
            html.setText("html:");

            head = new FormItem(html, SWT.NONE);
            head.setText("head:");

            title = new FormItem(head, SWT.NONE);
            title.setText("title:");
            Text titleText = new Text(title.getContent(), SWT.BORDER);
            titleText.setText("Hello World");
            title.setEditor(titleText);

            body = new FormItem(html, SWT.NONE);
            body.setText("body:");

            editor = new FormEditor(form, SWT.NONE);
            editor.setItems(HTML_TAGS, HTML_INDENT);
            editor.select(0);

            return form;
        }

        public int getReturnCode() {
            Shell shell = getShell();

            if (Boolean.getBoolean("com.osisc.junit.interactive")) {
                while (!shell.isDisposed()) {
                    if (!shell.getDisplay().readAndDispatch()) {
                        shell.getDisplay().sleep();
                    }
                }
            } else {
                while (shell.getDisplay().readAndDispatch());
            }
            return super.getReturnCode();
        }
    }

    @Before
    public void setup() {
        dialog = new TestDialog();
    }

    @After
    public void tearDown() {
        if (dialog.getShell() != null) {
            dialog.getShell().dispose();
        }
    }

    @Test
    public void testCreate() {
        dialog.open();
        dialog.getShell().setText("testCreate");

        assertArrayEquals(new Object[] { docType, html }, form.getItems());
        assertArrayEquals(new Object[] {}, docType.getItems());
        assertArrayEquals(new Object[] { head, body }, html.getItems());
        assertArrayEquals(new Object[] { title }, head.getItems());

        assertTrue(form.assertState());
        assertEquals(Dialog.OK, dialog.getReturnCode());
    }

    @Test
    public void testAddItem() {
        dialog.open();
        dialog.getShell().setText("testAddItem");

        h1 = new FormItem(body, SWT.NONE);
        h1.setText("h1:");
        Text h1Text = new Text(h1.getContent(), SWT.BORDER);
        h1Text.setText("Hello World");
        h1.setEditor(h1Text);

        assertArrayEquals(new Object[] { h1 }, body.getItems());
        assertArrayEquals(new Object[] {}, h1.getItems());

        assertTrue(form.assertState());
        assertEquals(Dialog.OK, dialog.getReturnCode());
    }

    @Test
    public void testDeleteItem() {
        dialog.open();
        dialog.getShell().setText("testDeleteItem");

        head.dispose();

        assertArrayEquals(new Object[] { docType, html }, form.getItems());
        assertArrayEquals(new Object[] {}, docType.getItems());
        assertArrayEquals(new Object[] { body }, html.getItems());

        assertTrue(form.assertState());
        assertEquals(Dialog.OK, dialog.getReturnCode());
    }

    @Test
    public void testEditorAddItem() {
        dialog.open();
        dialog.getShell().setText("testEditorAddItem");

        final SelectionEvent event[] = new SelectionEvent[2];

        editor.addSelectionListener(new SelectionListener() {
            public void widgetSelected(SelectionEvent e) {
                event[0] = e;
            }
            public void widgetDefaultSelected(SelectionEvent e) {
                event[1] = e;
            }
        });

        editor.setText("h1:", 2);
        editor.notifyListeners(SWT.Selection, null);

        assertNotNull(event[0]);
        assertSame(editor, event[0].widget);
        assertNull(event[0].item);
        assertTrue(event[0].doit);

        assertNull(event[1]);

        assertEquals("h1:", editor.getText());
        assertEquals(2, editor.getLevel());

        assertTrue(form.assertState());
        assertEquals(Dialog.OK, dialog.getReturnCode());
    }
}
