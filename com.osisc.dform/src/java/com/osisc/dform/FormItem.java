/*******************************************************************************
 * Copyright (c) 2016 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/

package com.osisc.dform;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Label;

/**
 * @author mattias
 */
public class FormItem extends Item {
    private final CLabel label;
    private Control editor;
    private final Button deleteButton;
    private final List items = new ArrayList();
    private final FormItem parentItem;

    public FormItem(Form parent, int style) {
        this(parent, null, style);
    }
    public FormItem(FormItem parent, int style) {
        this(parent.getForm(), parent, style);
    }

    private FormItem(Form form, FormItem parent, int style) {
        super(form, style);

        label = new CLabel(form.getContent(), SWT.NONE);
        GridData data = new GridData();
        data.horizontalIndent = (parent != null) ? parent.getIndent() + 1 : 0;
        data.horizontalIndent *= form.getIndentPixels();
        label.setLayoutData(data);

        editor = new Label(form.getContent(), SWT.NONE);
        editor.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

        if ((style & SWT.READ_ONLY) == 0) {
            deleteButton = new Button(form.getContent(), SWT.PUSH);
            deleteButton.setImage(form.getDeleteImage());
            deleteButton.addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    deleteButtonSelected(e);
                }
            });
        } else {
            deleteButton = null;
        }

        parentItem = parent;

        if (parent != null) {
            parent.createItem(this);
        }
        form.createItem(this);
    }

    public boolean assertState() {
        checkWidget();
        assert (label != null);
        assert (editor != null);
        if ((getStyle() & SWT.READ_ONLY) == 0) {
            assert (deleteButton != null);
        }
        for (Object item: items) {
            assert (((FormItem)item).getParentItem() == this);
        }
        return true;
    }

    Form getForm() { return (Form)label.getParent().getParent(); }
    public Composite getContent() { return label.getParent(); }

    void createItem(FormItem item) { items.add(item); }
    void removeItem(FormItem item) { items.remove(item); }

    public FormItem[] getItems() {
        return (FormItem[])items.toArray(new FormItem[items.size()]);
    }
    public FormItem getParentItem() {
        return parentItem;
    }

    int getIndent() {
        int i = 0;

        for(FormItem p = getParentItem(); p != null; p = p.getParentItem()) {
            i++;
        }
        return i;
    }

    public void setText(String text) {
        label.setText(text);
        label.getParent().layout(new Control[] { label });
    }
    public String getText() { return label.getText(); }

    public void setImage(Image image) {
        label.setImage(image);
        label.getParent().layout(new Control[] { label });
    }
    public Image getImage() { return label.getImage(); }

    Control getLabel() { return label; }
    Control getDeleteButton() { return deleteButton; }

    public void setEditor(Control editor) {
        checkWidget();
        if (editor == null) {
            throw new NullPointerException();
        }
        if (this.editor != null) {
            this.editor.dispose();
        }
        this.editor = editor;

        editor.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
        editor.moveBelow(label);
        label.getParent().layout();
    }
    public Control getEditor() { return editor; }

    void moveAbove(Control control) {
        label.moveAbove(control);
        editor.moveAbove(control);
        if (deleteButton != null) {
            deleteButton.moveAbove(control);
        }
    }

    void deleteButtonSelected(SelectionEvent e) {
        dispose();
    }

    void setSelected(boolean select) {
    }

    interface Visitor {
        public boolean visit(FormItem item);
    }

    void accept(Visitor v) {
        if (v.visit(this)) {
            for (Object item: items) {
                ((FormItem)item).accept(v);
            }
        }
    }

    class DisposeVisitor implements Visitor {
        public boolean visit(FormItem item) {
            item.getLabel().dispose();
            item.getEditor().dispose();
            if (item.getDeleteButton() != null) {
                item.getDeleteButton().dispose();
            }

            item.disposeItem();
            return true;
        }
    }

    void disposeItem() { super.dispose(); }

    public void dispose() {
        Form form = getForm(); // Get parent before we dispose

        accept(new DisposeVisitor());

        if (parentItem != null) {
            parentItem.removeItem(this);
        }
        form.removeItem(this);
    }
}
