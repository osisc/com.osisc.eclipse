/*******************************************************************************
 * Copyright (c) 2016 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/

package com.osisc.dform;


import java.util.List;

import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Widget;


/**
 * @author mattias
 */
public class FormViewer extends StructuredViewer {
    private Form form;

    public FormViewer() {
    }

    public Control getControl() { return form; }

    protected Widget doFindInputItem(Object element) {
        if (element != null && equals(element, getRoot())) {
            return getControl();
        }
        return null;
    }

    protected Widget doFindItem(Object element) {
        if (element != null) {
            for (FormItem item: form.getItems()) {
                if (item.getData() == element) {
                    return item;
                }
            }
        }
        return null;
    }

    protected void doUpdateItem(Widget item, Object element, boolean fullMap) {
         // TODO Auto-generated method stub
    }

    protected List getSelectionFromWidget() {
        return null; // TODO Auto-generated method stub
    }

    protected void setSelectionToWidget(List l, boolean reveal) {
        // TODO Auto-generated method stub
    }

    protected void internalRefresh(Object element) {
         // TODO Auto-generated method stub
    }

    public void reveal(Object element) {
        // TODO Auto-generated method stub
   }
}
