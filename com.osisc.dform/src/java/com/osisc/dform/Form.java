/*******************************************************************************
 * Copyright (c) 2016 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/

package com.osisc.dform;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * @author mattias
 */
public class Form extends ScrolledComposite {
    private int indent;
    private List items = new ArrayList(1);
    private List selected = new ArrayList(1);
    private FormEditor editor;
    private ResourceManager resources;

    public Form(Composite parent, int style) {
        super(parent, checkStyle(style));

        Composite content = new Composite(this, SWT.NONE);
        content.setLayout(new GridLayout(readOnly() ? 2 : 3, false));

        super.setExpandHorizontal(true);
        super.setExpandVertical(true);
        super.setContent(content);

        resources = new LocalResourceManager(JFaceResources.getResources(), this);
        indent = createIndent(); // TODO: Handle font changes
    }

    private static int checkStyle(int style) {
        if ((style & SWT.NO_SCROLL) == 0) {
            style |= SWT.H_SCROLL | SWT.V_SCROLL;
        }
        if ((style & SWT.H_SCROLL) != 0) {
            style |= SWT.V_SCROLL;
        }
        return style;
    }

    boolean readOnly() {
        return (getStyle() & SWT.READ_ONLY) != 0;
    }

    private class AssertVisitor implements FormItem.Visitor {
        int i = 0;
        Control[] controls = getContent().getChildren();

        AssertVisitor() {}

        public boolean visit(FormItem item) {
            assert (item.assertState());
            assert (item.getLabel() == controls[i++]);
            assert (item.getEditor() == controls[i++]);
            assert (readOnly() || item.getDeleteButton() == controls[i++]);
            return true;
        }
    }

    public boolean assertState() {
        AssertVisitor v = new AssertVisitor();

        checkWidget();
        for (Object item: items) {
            assert (((FormItem)item).getParentItem() == null);
            ((FormItem)item).accept(v);
        }
        if (editor != null) {
            assert (editor.assertState());
            assert (editor.getLabel() == v.controls[v.i++]);
            assert (editor.getAddButton() == v.controls[v.i++]);
        }
        return true;
    }

    static String INDENT_STRING = "        ";

    private int createIndent() {
        GC graphics = new GC(this);

        try {
            return graphics.textExtent(INDENT_STRING).x;
        } finally {
            graphics.dispose();
        }
    }

    public Composite getContent() {
        return (Composite)super.getContent();
    }
    public void setContent(Control control) {
        throw new UnsupportedOperationException();
    }
    public void setExpandHorizontal(boolean expand) {
        throw new UnsupportedOperationException();
    }
    public void setExpandVertical(boolean expand) {
        throw new UnsupportedOperationException();
    }

    int getIndentPixels() { return indent; }

    public String getIndentString(int level) {
        checkWidget();
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < level; i++) {
            sb.append(Form.INDENT_STRING);
        }
        return sb.toString();
    }

    void createItem(FormItem item) {
        if (editor != null) {
            item.moveAbove(editor.getLabel());
        }
        if (item.getParentItem() == null) {
            items.add(item);
        }
        setMinSize(getContent().computeSize(SWT.DEFAULT, SWT.DEFAULT));
    }
    void removeItem(FormItem item) {
        if (item.getParentItem() == null) {
            items.remove(item);
        }
        getContent().layout(false);
        setMinSize(getContent().computeSize(SWT.DEFAULT, SWT.DEFAULT));
    }

    void createEditor(FormEditor editor) {
        if (this.editor != null) {
            this.editor.dispose();
        }
        this.editor = editor;
        setMinSize(getContent().computeSize(SWT.DEFAULT, SWT.DEFAULT));
    }

    private static ImageDescriptor addImage =
            ImageDescriptor.createFromFile(Form.class, "add_obj.gif");
    private static ImageDescriptor deleteImage =
            ImageDescriptor.createFromFile(Form.class, "delete_obj.gif");
    private static ImageDescriptor errorImage =
            ImageDescriptor.createFromFile(Form.class, "error_ovr.gif");
    private static ImageDescriptor warningImage =
            ImageDescriptor.createFromFile(Form.class, "warning_ovr.gif");

    Image getAddImage() { return resources.createImage(addImage); }
    Image getDeleteImage() { return resources.createImage(deleteImage); }
    public Image getWarningImage() { return resources.createImage(warningImage); }
    public Image getErrorImage() { return resources.createImage(errorImage); }

    public FormItem[] getItems() {
        checkWidget();
        return (FormItem[])items.toArray(new FormItem[items.size()]);
    }

    public FormEditor getEditor() {
        checkWidget();
        return editor;
    }

    public void setSelection(FormItem item) {
        checkWidget();
        if (item == null) {
            throw new NullPointerException();
        }
        setSelection(new FormItem[] { item });
    }

    public void setSelection(FormItem[] items) {
        deselectAll();
        for (FormItem item: items) {
            item.setSelected(true);
        }
    }

    public FormItem[] getSelection() {
        return new FormItem[0];
    }

    public int getSelectionCount() {
        return 0;
    }

    public void deselectAll() {
        for (Object item: selected) {
            ((FormItem)item).setSelected(false);
        }
    }
}
