/*******************************************************************************
 * Copyright (c) 2016 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/

package com.osisc.dform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

/**
 * A replacement for Label or CLabel that can participate in focus traversal.
 *
 * <p>
 * Note that although this class is a subclass of <code>Composite</code>,
 * it does not make sense to add children to it, or set a layout on it.
 * </p>
 *
 * @author mattias
 */
public class FormLabel extends Composite {
    private static final int GAP = 5;
    private static final int MARGIN = 3;

    private int align = SWT.LEAD;

    private Label image;
    private Label text;

    public FormLabel(Composite parent, int style) {
        super(parent, 0);

        image = new Label(this, checkImageStyle(style));
        text = new Label(this, checkTextStyle(style));

        addDisposeListener(new DisposeListener() {
            public void widgetDisposed(DisposeEvent e) {
                FormLabel.this.widgetDisposed(e);
            }
        });
        addControlListener(new ControlAdapter() {
            public void controlResized(ControlEvent e) {
                FormLabel.this.controlResized(e);
            }
        });

    }

    private static int checkImageStyle(int style) {
        int mask = SWT.LEAD | SWT.TRAIL | SWT.CENTER;
        style = checkBits(style, SWT.LEAD, SWT.TRAIL, SWT.CENTER);
        return style & mask;
    }

    private static int checkTextStyle(int style) {
        int mask = SWT.SEPARATOR
                | SWT.VERTICAL | SWT.HORIZONTAL
                | SWT.SHADOW_OUT | SWT.SHADOW_IN | SWT.SHADOW_NONE;

        if ((style & SWT.SEPARATOR) != 0) {
            style = checkBits(style, SWT.VERTICAL, SWT.HORIZONTAL, 0);
            style = checkBits(style, SWT.SHADOW_OUT, SWT.SHADOW_IN, SWT.SHADOW_NONE);
        }
        style = checkBits(style, SWT.LEAD, SWT.TRAIL, SWT.CENTER);
        return style & mask;
    }

    /**
     * @param bits The bits to validate
     * @param opt0 Priority 0 and default bit(s) to set
     * @param opt1 Priority 1 bit(s) to set
     * @param opt2 Priority 2 bit(s) to set
     * @return bits with one of opt0, opt1 or opt2 set.
     */
    static int checkBits(int bits, int opt0, int opt1, int opt2) {
        int style = bits & ~(opt0 | opt1 | opt2);

        if ((bits & opt0) != 0) {
            return style | opt0;
        } else if ((bits & opt1) != 0) {
            return style | opt1;
        } else if ((bits & opt2) != 0) {
            return style | opt2;
        } else {
            return style | opt0;
        }
    }

    public void setAlignment(int align) {
        checkWidget();
        if (align != SWT.LEFT && align != SWT.CENTER && align != SWT.RIGHT) {
            SWT.error(SWT.ERROR_INVALID_ARGUMENT);
        }
        if (this.align != align) {
            this.align = align;
            redraw();
        }
    }

    public int getAlignment() {
        return align;
    }

    public Image getImage() {
        return this.image.getImage();
    }

    public void setImage(Image image) {
        this.image.setImage(image);
        resize();
    }

    public String getText() {
        return this.text.getText();
    }

    public void setText(String text) {
        this.text.setText(text);
        resize();
    }

    private boolean hasImage() { return image.getImage() != null; }
    private boolean hasText() { return !text.getText().isEmpty(); }

    private void resize() {
        Point imageSize = image.computeSize(SWT.DEFAULT, SWT.DEFAULT, false);
        Point textSize = text.computeSize(SWT.DEFAULT, SWT.DEFAULT, false);
        image.setBounds(MARGIN, MARGIN, imageSize.x, imageSize.y);
        text.setBounds(imageSize.x + GAP, MARGIN, textSize.x, textSize.y);
    }

    public Point computeSize(int width, int height, boolean changed) {
        Point imageSize = image.computeSize(SWT.DEFAULT, SWT.DEFAULT, false);
        Point textSize = text.computeSize(SWT.DEFAULT, SWT.DEFAULT, false);
        int x = imageSize.x + GAP + textSize.x;
        int y = Math.max(imageSize.y, textSize.y);
        return new Point(2 * MARGIN + x, 2 * MARGIN + y);
    }

    void onPaint(PaintEvent event) {
    }

    void onTraverse(TraverseEvent e) {
    }

    void controlResized(ControlEvent e) {
        resize();
    }

    void widgetDisposed(DisposeEvent e) {
    }
}
