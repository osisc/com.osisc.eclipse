/*******************************************************************************
 * Copyright (c) 2016 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/

package com.osisc.dform;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TypedListener;

/**
 * @author mattias
 */
public class FormEditor extends Item {
    private final Combo label;
    private final Button addButton;
    private int[] levels;
    private String[] items;

    public FormEditor(Form form, int style) {
        super(form, style);

        label = new Combo(form.getContent(), SWT.DROP_DOWN | SWT.READ_ONLY);
        label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

        addButton = new Button(form.getContent(), SWT.PUSH);
        addButton.setImage(form.getAddImage());
        addButton.addListener(SWT.Selection, new Listener() {
            public void handleEvent(Event e) {
                addButtonSelected(e);
            }
        });
        form.createEditor(this);
    }

    public boolean assertState() {
        checkWidget();
        assert (label != null);
        assert (addButton != null);

        assert (items.length == levels.length);
        assert (items.length == label.getItemCount());
        for (int i = 0; i < items.length; i++) {
            assert (label.getItem(i).equals(getIndentString(levels[i]) + items[i]));
        }
        return true;
    }

    Control getLabel() { return label; }
    Control getAddButton() { return addButton; }

    private static String getIndentString(int level) {
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < level; i++) {
            sb.append(Form.INDENT_STRING);
        }
        return sb.toString();
    }

    public void setItems(String[] items, int[] levels) {
        if (items.length != levels.length) {
            throw new IllegalArgumentException(items.length + " != " + levels.length);
        }
        label.removeAll();
        for (int i = 0; i < items.length; i++) {
            label.add(getIndentString(levels[i]) + items[i]);
        }
        this.levels = new int[levels.length];
        System.arraycopy(levels, 0, this.levels, 0, levels.length);
        this.items = new String[items.length];
        System.arraycopy(items, 0, this.items, 0, items.length);
    }

    public void select(int i) { label.select(i); }

    public void setText(String text) {
        label.setText(text);
    }
    public void setText(String text, int level) {
        label.setText(getIndentString(level) + text);
    }

    void addButtonSelected(Event buttonEvent) {
        notifyListeners(SWT.Selection, new Event());
    }

    public void addSelectionListener(SelectionListener listener) {
        if (listener == null) {
            SWT.error(SWT.ERROR_NULL_ARGUMENT);
        }
        addListener(SWT.Selection, new TypedListener(listener));
    }

    public void removeSelectionListener(SelectionListener listener) {
        if (listener == null) {
            SWT.error(SWT.ERROR_NULL_ARGUMENT);
        }
        removeListener(SWT.Selection, listener);
    }

    public int getSelectionIndex() { return label.getSelectionIndex(); }

    public String getText() {
        int i = label.getSelectionIndex();

        if (i < 0 || i > items.length) {
            return "";
        }
        return items[i];
    }

    public int getLevel() {
        int i = label.getSelectionIndex();

        if (i < 0 || i > levels.length) {
            return -1;
        }
        return levels[i];
    }
}
