/*******************************************************************************
 * Copyright (c) 2016 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/
package com.osisc.java;

/**
 * @author mattias
 */
public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(Class from, String name) {
        super(toPath(from, name));
    }

    private static String toPath(Class from, String name) {
        if (from == null || name == null) {
            throw new NullPointerException();
        }
        if (!name.startsWith("/")) {
            String path = from.getName();
            int i = path.lastIndexOf('.');

            if (i >= 0) {
                path = path.substring(0, i + 1);
                return path.replace('.', '/') + name;
            } else {
                return name;
            }
        } else {
            return name.substring(1);
        }
    }
}
