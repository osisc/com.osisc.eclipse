/*******************************************************************************
 * Copyright (c) 2016 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/

package com.osisc.java.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.osisc.java.ResourceNotFoundException;

/**
 * @author mattias
 */
public class ResourceNotFoundExceptionTest {
    @Test
    public void testAbsolute() {
        Exception e = new ResourceNotFoundException(getClass(), "/foo.bar");
        assertEquals("foo.bar", e.getMessage());
    }

    @Test
    public void testRelative() {
        Exception e = new ResourceNotFoundException(getClass(), "foo.bar");
        assertEquals("com/osisc/java/test/foo.bar", e.getMessage());
    }

    @Test
    public void testDefault() throws Exception {
        Exception e = new ResourceNotFoundException(Class.forName("Null"), "foo.bar");
        assertEquals("foo.bar", e.getMessage());
    }
}
