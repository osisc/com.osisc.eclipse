/*******************************************************************************
 * Copyright (c) 2016 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/

package com.osisc.eclipse.ui.internal.tests;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.junit.Assert;
import org.junit.Test;

import com.osisc.eclipse.ui.internal.Plugin;

/**
 * @author mattias
 */
public class PluginTest {
    private static class A {}
    private static class B {}

    @Test
    public void testGetDialogSettings() {
        IDialogSettings a = Plugin.get().getDialogSettings(A.class);
        IDialogSettings b = Plugin.get().getDialogSettings(B.class);

        a.put("key", true);
        Assert.assertNotNull(a.get("key"));
        Assert.assertNull(b.get("key"));
    }
}
