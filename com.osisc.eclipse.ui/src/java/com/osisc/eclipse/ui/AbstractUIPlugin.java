/*******************************************************************************
 * Copyright (c) 2013-16 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/

package com.osisc.eclipse.ui;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.DialogSettings;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.PlatformUI;

import com.osisc.eclipse.internal.PluginLog;
import com.osisc.swt.SWTUtils;

/**
 * A common base class for Eclipse plugins minimizing code duplication.
 */
public abstract class AbstractUIPlugin
extends org.eclipse.ui.plugin.AbstractUIPlugin {
    private int logLevel = IStatus.WARNING;

    public abstract String id();

    public int logLevel() {
        return logLevel;
    }

    public void log(IStatus status) {
        PluginLog.log(logLevel(), id(), status);
    }
    public void log(int severity, String fmt, Object... args) {
        PluginLog.log(logLevel(), id(), severity, null, fmt, args);
    }
    public void log(int severity, Throwable e, String fmt, Object... args) {
        PluginLog.log(logLevel(), id(), severity, e, fmt, args);
    }

    public IStatus status(int severity, String msg) {
        return new Status(severity, id(), msg, null);
    }
    public IStatus status(int severity, Throwable e, String msg) {
        return new Status(severity, id(), msg, e);
    }
    public IStatus status(int severity, Enum code, String msg) {
        return new Status(severity, id(), code.ordinal(), msg, null);
    }
    public IStatus status(int severity, Enum code, Throwable e, String msg) {
        return new Status(severity, id(), code.ordinal(), msg, e);
    }

    public IDialogSettings getDialogSettings(Class c) {
        IDialogSettings plugin = super.getDialogSettings();
        return DialogSettings.getOrCreateSection(plugin, c.getName());
    }

    public void asyncExec(final Widget widget, final Runnable runnable) {
        Display display = PlatformUI.getWorkbench().getDisplay();
        SWTUtils.asyncExec(display, widget, runnable);
    }

    public void syncExec(final Widget widget, final Runnable runnable) {
        Display display = PlatformUI.getWorkbench().getDisplay();
        SWTUtils.syncExec(display, widget, runnable);
    }
}
