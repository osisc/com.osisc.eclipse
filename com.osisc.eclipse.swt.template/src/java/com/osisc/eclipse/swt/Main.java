package com.osisc.eclipse.swt;

import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.widgets.Display;

public class Main extends ApplicationWindow {

    private Main() {
        super(null);
        setBlockOnOpen(true);
    }

    public static void main(String[] args) {
        Main window = new Main();

        int result = window.open();

        Display.getCurrent().dispose();
        System.exit(result);
    }
}
