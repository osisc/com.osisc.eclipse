/*******************************************************************************
 * Copyright (c) 2013-15 Mattias Bertilsson and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Mattias Bertilsson - initial API and implementation
 *******************************************************************************/

package com.osisc.eclipse.plugin.internal;

import org.osgi.framework.BundleContext;

import com.osisc.eclipse.AbstractPlugin;

public class Plugin extends AbstractPlugin {
    private static Plugin plugin;
    private static final String PLUGIN_ID = "com.osisc.eclipse.plugin";

    public static enum StatusCode {};

    public void start(BundleContext context) throws Exception {
        super.start(context);
        plugin = this;
    }

    public static Plugin get() {
        return plugin;
    }
    public String id() {
        return PLUGIN_ID;
    }
}
